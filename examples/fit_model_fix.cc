/*
  -------------------------------------------------------------------
  
  Copyright (C) 2006, 2007, 2008, 2009, 2010, Andrew W. Steiner

  Small parts of the code are:
  Copyright (C) 2010, Edwin van Leeuwen

  Both original code and this code are released under the GPLv3
  
  -------------------------------------------------------------------
*/

#include <iostream>
#include <cmath>
#include <gsl/gsl_sf_bessel.h>
#include <o2scl/ovector_tlate.h>
#include <o2scl/multi_funct.h>
#include <o2scl/funct.h>
#include <o2scl/test_mgr.h>
#include <o2scl/gsl_rnga.h>

#include "o2scl/diff_evo.h"
#include "o2scl/multi_min_fix2.h"

using namespace std;
using namespace o2scl;

// A simple function with many local minima. A "greedy" minimizer
// would likely fail to find the correct minimum.
class cl {
    public:
        gsl_rnga gr;
        double function(size_t nvar, const ovector_base &x) {
            double y=(x[0]-2.0)*(x[0]-2.0)+(x[1]-1.0)*(x[1]-1.0)+x[2]*x[2];
            return y;
        }

        //We have three parameters and have different random function for both:
        int init_function( size_t dim, const ovector_base &x, 
                ovector_base &y) {
            y[0] = 20*gr.random()-40;
            y[1] = 10*gr.random()-20;
            y[2] = 15*gr.random()-30;
            return 0;
        }
};

int main(int argc, char *argv[]) {
  test_mgr t;
  cl acl;
  t.set_output_level(1);

  cout.setf(ios::scientific);

  diff_evo<multi_funct<>, ovector_base, mm_funct<> > de;
  double result;
  ovector init(3);
  
  multi_funct_mfptr<cl> fx(&acl, &cl::function);
  mm_funct_mfptr<cl, ovector_base> init_f( &acl, &cl::init_function );

  de.set_init_function( init_f );
  de.ntrial=2000;


  // Choose an initial point at a local minimum away from
  // the global minimum
  init[0]=9.0;
  init[1]=9.0;
  init[2]=9.0;
  
  // Perform the minimization
  de.mmin(3,init,result,fx);
  cout << "x: " << init[0] << " " << init[1] << " " << init[2]
       << ", minimum function value: " << result << endl;
  cout << endl;
 
  // Test that it found the global minimum
  t.test_rel(init[0],2.0,1.0e-4,"1a");
  t.test_rel(init[1],1.0,1.0e-4,"1b");
  t.test_rel(init[2],0.0,1.0e-4,"1c");


  // Fix the first variable
  multi_min_fix2<bool[3]> gmf;
  diff_evo<multi_funct_mfptr<multi_min_fix2<bool[3]> >, ovector_base, mm_funct<> > de2;
  de2.set_init_function( init_f );
  de2.ntrial=1000;
  de2.verbose=1;
  //de2.tolx=1.0e-7;

  gmf.set_mmin( de2 );

  bool fix[3] = {false, true, false};
  init[0]=0.5;
  init[1]=0.5;
  init[2]=0.5;
  gmf.mmin_fix(3, init, result, fix, fx);

  cout << "x: " << init[0] << " " << init[1] << " " << init[2]
       << ", minimum function value: " << result << endl;
  cout << endl;
 
  t.test_rel(init[0],2.0,1.0e-4,"3a");
  t.test_rel(init[1],0.5,1.0e-4,"3b");
  t.test_rel(init[2],0.0,1.0e-4,"3c");

  t.report();
  
  return 0;
}
// End of example

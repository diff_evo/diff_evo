/*
   -------------------------------------------------------------------

   Copyright (C) 2006, 2007, 2008, 2009, 2010, Andrew W. Steiner

   This file is part of O2scl.

   O2scl is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   O2scl is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with O2scl. If not, see <http://www.gnu.org/licenses/>.

   -------------------------------------------------------------------

*/
#ifndef O2SCL_MULTI_MIN_FIX_H
#define O2SCL_MULTI_MIN_FIX_H

#include <o2scl/multi_min.h>
#include <o2scl/gsl_mmin_simp.h>

#ifndef DOXYGENP
namespace o2scl {
#endif

    /** \brief Multidimensional minimizer fixing some variables and 
      varying others

      See an example for the usage of this class
      in \ref ex_mmin_fix_sect .

      \todo Generalize to all vector types
      \todo Generalize to minimizers which require derivatives
      \todo At the moment, the user has to change def_mmin::ntrial
      instead of multi_min_fix::ntrial, which is a bit confusing. 
      Fix this.

*/
#ifdef DOXYGENP
    template<class bool_vec_t>
        class multi_min_fix2 : public multi_min
#else
    template<class bool_vec_t>
        class multi_min_fix2 : public multi_min<multi_funct<> >
#endif
    {

        public:

            /** \brief Specify the member function pointer
            */
            multi_min_fix2() {
                mmp=&def_mmin;
            }

            virtual ~multi_min_fix2() {}

            /** \brief Calculate the minimum \c min of \c func w.r.t. the
              array \c x of size \c nvar.
              */
            virtual int mmin(size_t nvar, ovector_base &x, double &fmin, 
                    multi_funct<> &func) {

                // Copy arguments for later use
                funcp=&func;
                unv=nvar;
                nv_new=nvar;
                ovector xnew(nv_new);

                // Copy initial guess to new format
                for(size_t i=0;i<nvar;i++) {
                    xnew[i]=x[i];
                }

                // Perform minimization
                multi_funct_mfptr<multi_min_fix2> 
                    mfm(this,&multi_min_fix2::min_func);
                int ret=mmp->mmin(nv_new,xnew,fmin,mfm);
                if (ret!=0) {
                    O2SCL_ERR("Minimizer failed in multi_min_fix::mmin().",ret);
                }

                // Copy final result back to original format
                for(size_t i=0;i<nvar;i++) {
                    x[i]=xnew[i];
                }

                this->last_ntrial=mmp->last_ntrial;

                return ret;
            }

            /** \brief Calculate the minimum of \c func while fixing 
              some parameters as specified in \c fix.

              If all of entries <tt>fix[0], fix[1], ... fix[nvar-1]</tt>
              are true, then this function assumes all of the parameters
              are fixed and that there is no minimization to be performed.
              In this case, it will return 0 for success without calling
              the error handler. 
              */
            virtual int mmin_fix(size_t nvar, ovector_base &x, double &fmin, 
                    bool_vec_t &fix, 
                    multi_funct<> &func) {

                // Copy arguments for later use
                x_init.allocate( nvar );
                for (size_t i=0; i<nvar; ++i)
                    x_init[i] = x[i];
                funcp=&func;
                unv=nvar;
                fixp=&fix;
                nv_new=0;
                for(size_t i=0;i<nvar;i++) {
                    if (fix[i]==false) nv_new++;
                }
                if (nv_new==0) return 0;

                // Copy initial guess to new format
                size_t j=0;
                ovector xnew(nv_new);
                for(size_t i=0;i<nvar;i++) {
                    if (fix[i]==false) {
                        xnew[j]=x[i];
                        j++;
                    }
                }

                // Perform minimization
                multi_funct_mfptr<multi_min_fix2> 
                    mfm(this,&multi_min_fix2::min_func);
                int ret=mmp->mmin(nvar,x,fmin,mfm);
                if (ret!=0) {
                    O2SCL_ERR("Minimizer failed in multi_min_fix::mmin_fix().",ret);
                }

                // Copy final result back to original format
                j=0;
                for(size_t i=0;i<nvar;i++) {
                    if (fix[i]==true) {
                        x[i]=x_init[i];
                    }
                }

                this->last_ntrial=mmp->last_ntrial;

                return ret;
            }

            /// Change the base minimizer
            int set_mmin(multi_min<multi_funct_mfptr
                    <multi_min_fix2> > &min) {
                mmp=&min;
                return 0;
            }

            /// The default base minimizer
            gsl_mmin_simp<multi_funct_mfptr<multi_min_fix2> > def_mmin;

#ifndef DOXYGEN_INTERNAL

        protected:

            /** \brief The new function to send to the minimizer
            */
            virtual double min_func(size_t nv, const ovector_base &x ) {
                ovector tmp(unv);
                for(size_t i=0;i<unv;i++) {
                    if (nv_new<unv && (*fixp)[i]==true) {
                        tmp[i]=x_init[i];
                    } else {
                        tmp[i]=x[i];
                    }
                }
                return (*funcp)(unv,tmp);
            }

            /// The minimizer
            multi_min<multi_funct_mfptr<multi_min_fix2> > *mmp;

            /// The user-specified function
            multi_funct<> *funcp;

            /// The user-specified number of variables
            size_t unv;

            /// The new number of variables
            size_t nv_new;

            /// Specify which parameters to fix
            bool_vec_t *fixp;

            /// The user-specified initial vector
            ovector x_init;

#ifndef DOXYGENP
#endif

        private:

            multi_min_fix2(const multi_min_fix2 &);
            multi_min_fix2& operator=(const multi_min_fix2&);

#endif

    };


#ifndef DOXYGENP
}
#endif

#endif

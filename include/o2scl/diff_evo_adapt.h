/* -------------------------------------------------------------------

	 Copyright (C) 2006-2012, Andrew W. Steiner
	 Copyright (C) 2010-2012, Edwin van Leeuwen

	 This file is part of O2scl.

	 O2scl is free software; you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation; either version 3 of the License, or
	 (at your option) any later version.

	 O2scl is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with O2scl. If not, see <http://www.gnu.org/licenses/>.

	 -------------------------------------------------------------------
	 */
#ifndef O2SCL_DIFF_EVO_ADAPT_H
#define O2SCL_DIFF_EVO_ADAPT_H
#include <vector>
#include <algorithm>

#include <o2scl/gsl_rnga.h>
#include <o2scl/mm_funct.h>

#include <o2scl/diff_evo.h>

#ifndef DOXYGENP
namespace o2scl {
#endif
	/**  \brief Multidimensional minimization by the differential evolution method

		This class minimizes a function using differential evolution. This method is
		a genetic algorithm and as such works well for non continuous problems, since
		it does not rely on a gradient of the function that is being minimized.

		This is an adaptive version of the standard differential evolution as implemented
		in \ref diff_evo .
		Adaptation is implemented according to:

		Brest, J., S. Greiner, B. Boskovic, M. Mernik, and V. Zumer. 2006. Self-Adapting Control Parameters in Differential Evolution: A Comparative Study on Numerical Benchmark Problems. IEEE Transactions on Evolutionary Computation 10 (6) (December): 646 657. doi:10.1109/TEVC.2006.872133.
		*/
#ifdef DOXYGENP
	template<class func_t=multi_funct<>, 
		class vec_t=ovector_base, class init_funct_t=mm_funct_fptr<vec_t >,
		class alloc_vec_t=ovector, class alloc_t=ovector_alloc > 
			class diff_evo_adapt :  public diff_evo
#else
	template<class func_t=multi_funct<>, 
		class vec_t=ovector_base, class init_funct_t=mm_funct_fptr<vec_t >,
		class alloc_vec_t=ovector, class alloc_t=ovector_alloc > 
			class diff_evo_adapt :  public diff_evo<func_t, vec_t, init_funct_t,
																alloc_vec_t, alloc_t>
#endif
	{
		public:
			/**
			 * \brief Probability of adapting F and CR respecitively
			 */
			double tau_1, tau_2;

			/**
			 * \brief Lower bound and range of F
			 */
			double fl, fr;

			diff_evo_adapt() : diff_evo<func_t, 
			vec_t, init_funct_t, alloc_vec_t, alloc_t>() {
				tau_1 = 0.1;
				tau_2 = 0.1;
				fl = 0.1;
				fr = 0.9;
			}

			virtual int mmin(size_t nvar, vec_t &x0, double &fmin,
					func_t &func) {
				//keep track of number of generation without better solutions
				int nconverged = 0;
				if (this->pop_size==0) {
					//automatically select pop_size dependent on dimensionality.
					this->pop_size = 10*nvar;
				}

				initialize_population( nvar, x0 );
				//Set initial fmin
				for (size_t x = 0; x < this->pop_size; ++x) {
					alloc_vec_t agent_x;
					this->ao.allocate( agent_x, nvar );
					for (size_t i = 0; i < nvar; ++i) {
						agent_x[i] = this->population[x*nvar+i];
					}
					double fmin_x = 0;
					fmin_x=func(nvar,agent_x);
					fmins.push_back( fmin_x );
					if (x==0) {
						fmin = fmin_x;
						for (size_t i = 0; i<nvar; ++i)  
							x0[i] = agent_x[i];
						//x0 = agent_x;
					} else if (fmin_x<fmin) {
						fmin = fmin_x;
						for (size_t i = 0; i<nvar; ++i)  
							x0[i] = agent_x[i];
						//x0 = agent_x;
					}
					this->ao.free( agent_x );

				}

				int gen = 0;
				while (gen < this->ntrial && nconverged <= this->nconv) {
					++nconverged;
					++gen;

					//For each agent x in the population do: 
					for (size_t x = 0; x < this->pop_size; ++x) {

						std::vector<int> others;

						//create a copy agent_x and agent_y of the current agent vector
						alloc_vec_t agent_x, agent_y;
						this->ao.allocate( agent_x, nvar );
						this->ao.allocate( agent_y, nvar );
						for (size_t i = 0; i < nvar; ++i) {
							agent_x[i] = this->population[x*nvar+i];
							agent_y[i] = this->population[x*nvar+i];
						}
						double f, cr;
						if (this->gr.random() >= tau_1)
							f = variables[x*2];
						else
							f = fl+this->gr.random()*fr;
						if (this->gr.random() >= tau_2)
							cr = variables[x*2+1];
						else
							cr = this->gr.random();


						//Pick three agents a, b, and c from the population at random, they must be distinct from each other as well as from agent x
						others = this->pick_unique_agents( 3, x );

						//Pick a random index R � {1, ..., n}, where the highest possible value n is the dimensionality of the problem to be optimized.
						size_t r = floor(this->gr.random()*nvar);

						for (size_t i = 0; i < nvar; ++i) {
							//Pick ri~U(0,1) uniformly from the open range (0,1)
							double ri = this->gr.random();
							//If (i=R) or (ri<CR) let yi = ai + F(bi - ci), otherwise let yi = xi
							if (i == r || ri < cr) {
								agent_y[i] = this->population[others[0]*nvar+i] + 
									f*(this->population[others[1]*nvar+i]-
											this->population[others[2]*nvar+i]);
							}
						}
						//If (f(y) < f(x)) then replace the agent in the population with the improved candidate solution, that is, set x = y in the population
						//double fmin_x, fmin_y;
						double fmin_y;

						fmin_y=func(nvar,agent_y);
						if (fmin_y<fmins[x]) {
							for (size_t i = 0; i < nvar; ++i) {
								this->population[x*nvar+i] = agent_y[i];
								fmins[x] = fmin_y;
							}

							variables[x*2] = f;
							variables[x*2+1] = cr;

							if (fmin_y<fmin) {
								fmin = fmin_y;
								for (size_t i = 0; i<nvar; ++i) {  
									x0[i] = agent_y[i];
								}
								nconverged = 0;
							}
						}

						this->ao.free( agent_x );
						this->ao.free( agent_y );

					}
					if (this->verbose > 0)
						this->print_iter( nvar, fmin, gen, x0 );
				}
				if(gen>=this->ntrial) {
					std::string str="Exceeded maximum number of iterations ("+
						itos(this->ntrial)+") in diff_evo::mmin().";
					O2SCL_CONV_RET(str.c_str(),gsl_emaxiter,this->err_nonconv);
				}
				return 0;
			};

			virtual void print_iter( size_t nvar, double fmin, 
					int iter, vec_t &best_fit ) {
				std::cout << "Generation " << iter << std::endl;
				std::cout << "Fmin: " << fmin << std::endl;
				std::cout << "Parameters: ";
				for (size_t i=0; i<nvar; ++i) {
					std::cout << best_fit[i] << " ";
				}
				std::cout << std::endl;
				std::cout << "Population: " << std::endl;
				for (size_t i=0; i<this->pop_size; ++i ) {
					std::cout << i << ": ";
					for (size_t j = 0; j<nvar; ++j ) {
						std::cout << this->population[i*nvar+j] << " ";
					}
					std::cout << "fmin: " << fmins[i] << 
						" F: " << variables[i*2] <<
						" CR: " << variables[i*2+1] << std::endl;
				}
			}


#ifndef DOXYGEN_INTERNAL
		protected:
			/**
			 * \brief Vector containing the tunable variable F and CR
			 */
			alloc_vec_t variables;

			//Vector that keeps track of fmins values
			ovector fmins;

			/**
			 * \brief Initialize a population of random agents
			 */
			virtual int initialize_population( size_t nvar, vec_t &x0 ) {
				if (this->rand_init_funct==NULL) {
					O2SCL_ERR_RET("No initialization function provided.",
							gsl_ebadfunc );

				}
				this->ao.allocate( this->population, nvar*this->pop_size );
				this->ao.allocate( variables, 2*this->pop_size );
				for (size_t i = 0; i < this->pop_size; ++i) {
					alloc_vec_t y;
					this->ao.allocate( y, nvar );
					(*this->rand_init_funct)( nvar, x0, y );
					for (size_t j = 0; j < nvar; ++j) {
						this->population[ i*nvar+j ] = y[j];

					}
					this->ao.free(y);

					variables[i*2] = fl + this->gr.random()*fr;
					variables[i*2+1] = this->gr.random();
				}
				return 0;
			}
#endif
	};
#ifndef DOXYGENP
}
#endif

#endif
